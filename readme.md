# Cookielaw Notification jQuery Plugin v2

## Prerequisits

* jQuery 1.4+

## Install

* Include jQuery
* on DomReady call the plugin with the below possible options

```javascript
$.cookielaw({
    theme   :	"dark",
    msg		:	"By continuing to use this website you\'re agreeing to allow us to store cookies on your system.",
    link 	:	"/cookies",
    button	:	"Agree &amp; Continue",
    width	:   940,
    reset	:	$('.forget')
});
```

## Customising

Change the "theme" parameter to a new classname eg. "modern", then edit cookielaw.php or your own css file and follow the below classes to modify

```css
.modern.cookie-notification-wrapper {}
.modern .cookie-notification-message {}
.modern .cookie-notification-link {}
.modern .cookie-notification-button {}
```

**Note:** The class .cookie-notification.wrapper is joined with the theme name, where as all others should be children exactly as the demo shows.