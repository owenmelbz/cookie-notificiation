<?php
/* Cookie Law Plugin v2 - Selesti
 * Author: Owen Melbourne
 */
 if ( isset($_GET['css']) ){
 header("Content-type: text/css"); ?>
/* Default Layout Styles
 * Dont Edit These
 */
.cookie-notification-wrapper {display: none; width: 100%; margin: 0 auto 10px auto; font-size: 16px; overflow: auto; clear:both; background-color: white; box-sizing: border-box; -moz-box-sizing: border-box; padding: 10px; }
.cookie-notification-message {display: block; float: left; width: 80%; margin: 0; text-indent: 5px; font-family: 'Arial', sans-serif; color: #222;}
.cookie-notification-link {text-decoration: none; font-weight: 700; color: inherit;}
.cookie-notification-link:hover { color: #AAA; }
.cookie-notification-button {display: block; float: right; width: 20%; margin-top: 10px; max-width: 150px;margin-top:0; text-align: center; background: #8AC007; border: none; color: white; padding: 5px 10px; box-sizing: border-box; }
.cookie-notification-button:hover {background: #688F09;}

/* Theme Styles
 * Add your own theme in the same method as the "light" theme
 * .theme-name.cookie-notification-wrapper {}
 * .theme-name .cookie-notification-message {}
 * .theme-name .cookie-notification-link {}
 * .theme-name .cookie-notification-button {}
 */
/* Default Light Theme */
.light.cookie-notification-wrapper {border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; font-size: 14px;}
.light .cookie-notification-button { -moz-box-sizing: border-box; border-radius: 7px;}

/* Default Dark Theme */
.dark.cookie-notification-wrapper {border-bottom-left-radius: 10px; border-bottom-right-radius: 10px; font-size: 14px; background: #0D0D0D; }
.dark .cookie-notification-message {color: #F1F1F1; }
.dark .cookie-notification-button { -moz-box-sizing: border-box; border-radius: 7px;}

/* Custom Theme*/

<?php }
//the javascript
if ( isset($_GET['js']) ){
header("Content-type: text/javascript"); ?>
(function($){
	$.cookielaw = $.fn.cookielaw = function(options){
		cookie = {
			settings : $.extend({
					theme	:	"light",
					msg		:	"By continuing to use this website you\'re agreeing to allow us to store cookies on your system.",
					link 	:	"/cookies",
					button	:	"Agree &amp; Continue",
					width	:   940,
					reset	:	$('.cookie-notification-reset')
				}, options),
			init : function(){
				var data = cookie.localStorage.getItem('cookiedata');
				cookie.settings.reset.on('click', cookie.reset);
				if ( data == null || !data ) { cookie.notification(); }
				else { cookie.agreed(); }
			},
			notification: function(){
				var html = 		  '<div id="cookie-notification" class="cookie-notification-wrapper ' + cookie.settings.theme + '" style="max-width: ' + cookie.settings.width + 'px">';
					html = html + '<p class="cookie-notification-message">' + cookie.settings.msg + ' <a class="cookie-notification-link" href="' + cookie.settings.link + '" title="Read our full cookie policy" >More on cookies</a></p>';
					html = html + '<button class="cookie-notification-button">' + cookie.settings.button + '</button>';
					html = html + '</div>';		
				cookie.build(html);
			},
			agreed: function(){
				//Any things that need doing if the are already accepted.
			},
			build: function(string){
				$('body').prepend(string);
				$('.cookie-notification-wrapper').slideDown();
				$('.cookie-notification-button').on('click', cookie.accept);
			},
			accept: function(){
				cookie.localStorage.setItem('cookiedata', true);
				$('.cookie-notification-wrapper').slideUp(function(){
					$(this).remove();
				});
			},
			reset: function(){
				cookie.localStorage.removeItem('cookiedata');
				window.location = window.location;
			},
			localStorage: {
				setItem: function(key, value){
					try {
						console.log()
						return localStorage.setItem(key, value);
					}
					catch(e){console.warn('No localStorage support');}
				},
				removeItem: function(key){
					try {
						return localStorage.removeItem(key);
					}
					catch(e){console.warn('No localStorage support');}
				},
				getItem: function(key){
					try {
						return localStorage.getItem(key);
					}
					catch(e){console.warn('No localStorage support');}
				}
			}
		};
		cookie.init();
	};
}( jQuery ));
<?php } ?>